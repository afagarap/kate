# k-Competitive Autoencoder
# Copyright (C) 2021  Abien Fred Agarap
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""Autoencoding module for baseline and KATE"""
import argparse

import matplotlib.pyplot as plt
import numpy as np
from pt_datasets import load_dataset, create_dataloader, encode_features
import torch

from kate.models import Autoencoder
from kate.utils import export_model, set_global_seed, visualize_features


__author__ = "Abien Fred Agarap"


def visualize_images(top_images: torch.Tensor, bottom_images: torch.Tensor) -> None:
    """
    Plots the given images.

    Parameters
    ----------
    top_images: torch.Tensor
        The images to plot on the top row.
    bottom_images: torch.Tensor
        The images to plot on the bottom row.
    """
    number = 16
    plt.figure(figsize=(20, 4))
    for index in range(number):
        ax = plt.subplot(2, number, index + 1)
        plt.imshow(top_images[index].reshape(28, 28))
        plt.gray()
        ax.get_xaxis().set_visible(False)
        ax.get_yaxis().set_visible(False)
        ax = plt.subplot(2, number, index + 1 + number)
        plt.imshow(bottom_images[index].reshape(28, 28))
        plt.gray()
        ax.get_xaxis().set_visible(False)
        ax.get_yaxis().set_visible(False)
    plt.show()


def parse_args():
    parser = argparse.ArgumentParser(description="KATE")
    group = parser.add_argument_group("Parameters")
    group.add_argument(
        "-s",
        "--seed",
        type=int,
        default=42,
        help="the random to seed to use, default: [42]",
    )
    group.add_argument(
        "-d",
        "--dataset",
        type=str,
        default="mnist",
        help="the dataset to use, default: [mnist]",
    )
    group.add_argument(
        "-v",
        "--vectorizer",
        type=str,
        default="ngrams",
        help="the vectorizer to use, options: [ngrams (default) | tfidf]",
    )
    group.add_argument(
        "-nr",
        "--ngram_range",
        nargs="+",
        required=False,
        default=(1, 5),
        type=int,
        help="the n-grams range to use for text vectorization, default: [(1, 5)]",
    )
    group.add_argument(
        "-b",
        "--batch_size",
        type=int,
        default=256,
        help="the mini-batch size to use, default: [256]",
    )
    group.add_argument(
        "-c",
        "--code_dim",
        type=int,
        default=70,
        help="the latent code dimensionality, default: [70]",
    )
    group.add_argument(
        "-e",
        "--epochs",
        type=int,
        default=10,
        help="the number of epochs to train the model, default: [10]",
    )
    group.add_argument(
        "-uc",
        "--use_competitive_layer",
        required=False,
        dest="use_competitive_layer",
        action="store_true",
    )
    group.add_argument(
        "-k",
        "--top_k",
        required=False,
        default=32,
        type=int,
        help="the top-k winners to get, default: [32]",
    )
    group.add_argument(
        "-uwt",
        "--use_weight_tying",
        required=False,
        dest="use_weight_tying",
        action="store_true",
    )
    group.add_argument(
        "-a",
        "--amplification_factor",
        required=False,
        default=6.26,
        type=float,
        help="the amplification factor to use, default: [6.26]",
    )
    group.set_defaults(use_competitive_layer=False)
    group.set_defaults(use_weight_tying=False)
    arguments = parser.parse_args()
    return arguments


def main(arguments):
    (
        seed,
        dataset,
        vectorizer,
        ngram_range,
        batch_size,
        code_dim,
        epochs,
        use_competitive_layer,
        top_k,
        use_weight_tying,
        amplification_factor,
    ) = (
        arguments.seed,
        arguments.dataset,
        arguments.vectorizer,
        arguments.ngram_range,
        arguments.batch_size,
        arguments.code_dim,
        arguments.epochs,
        arguments.use_competitive_layer,
        arguments.top_k,
        arguments.use_weight_tying,
        arguments.amplification_factor,
    )

    set_global_seed(seed=seed)

    train_data, test_data = load_dataset(
        dataset, normalize=False, vectorizer=vectorizer, ngram_range=ngram_range
    )

    if dataset in ["malimg", "wdbc"]:
        num_features = train_data.tensors[0].shape[1]
        test_size = test_data.tensors[0].shape[0]
        classes = [str(index) for index in np.unique(test_data.tensors[1])]
    else:
        num_features = np.prod(train_data.data.shape[1:])
        test_size = test_data.data.shape[0]
        classes = train_data.classes

    train_loader = create_dataloader(train_data, batch_size=batch_size, num_workers=4)
    test_loader = create_dataloader(test_data, batch_size=test_size)

    model = Autoencoder(
        input_shape=num_features,
        code_dim=code_dim,
        use_competitive_layer=use_competitive_layer,
        top_k=top_k,
        use_weight_tying=use_weight_tying,
        amplification_factor=amplification_factor,
    )
    model.fit(train_loader, epochs=epochs)
    model = model.cpu()
    model.save_model(filename=f"Autoencoder-{dataset}-{code_dim}-dim-competitive_layer")

    with torch.no_grad():
        model = model.eval()
        model = model.cpu()
        for test_features, test_labels in test_loader:
            reconstructions = model.predict(test_features)

    if "mnist" in dataset:
        visualize_images(
            top_images=model.encoder[0].weight.cpu().detach().numpy(),
            bottom_images=model.decoder[len(model.decoder) - 2]
            .weight.transpose(0, 1)
            .cpu()
            .detach()
            .numpy(),
        )
        with torch.no_grad():
            model = model.eval()
            model = model.cpu()
            for test_features, test_labels in test_loader:
                reconstructions = model.predict(test_features)
        visualize_images(test_features, reconstructions)
        classes = train_data.classes
    latent_code = model.compute_latent_code(features=test_features)
    latent_code = latent_code.detach().numpy()
    encoded_latent_code = encode_features(
        latent_code, dim=2, encoder="tsne", use_cuda=True
    )
    visualize_features(
        encoded_latent_code,
        test_labels,
        classes=classes,
        title=f"{dataset.upper()} classes",
    )


if __name__ == "__main__":
    arguments = parse_args()
    main(arguments)
