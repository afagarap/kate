# k-Competitive Autoencoder
# Copyright (C) 2021  Abien Fred Agarap
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""Classifier module"""
import argparse

import numpy as np
from pt_datasets import load_dataset, create_dataloader, create_dataset
import torch

from kate.models import Autoencoder, SoftmaxRegression
from kate.utils import set_global_seed


__author__ = "Abien Fred Agarap"


def parse_args():
    parser = argparse.ArgumentParser(description="Regression classifier")
    group = parser.add_argument_group("Parameters")
    group.add_argument(
        "-s",
        "--seed",
        type=int,
        default=42,
        help="the random to seed to use, default: [42]",
    )
    group.add_argument(
        "-d",
        "--dataset",
        type=str,
        default="mnist",
        help="the dataset to use, default: [mnist]",
    )
    group.add_argument(
        "-b",
        "--batch_size",
        type=int,
        default=256,
        help="the mini-batch size to use, default: [256]",
    )
    group.add_argument(
        "-e",
        "--epochs",
        type=int,
        default=10,
        help="the number of epochs to train the model, default: [10]",
    )
    group.add_argument(
        "-c",
        "--code_dim",
        type=int,
        default=70,
        help="the latent code dimensionality, default: [70]",
    )
    group.add_argument(
        "-uc",
        "--use_competitive_layer",
        required=False,
        dest="use_competitive_layer",
        action="store_true",
    )
    arguments = parser.parse_args()
    return arguments


def main(arguments):
    (seed, dataset, batch_size, epochs, code_dim, use_competitive_layer) = (
        arguments.seed,
        arguments.dataset,
        arguments.batch_size,
        arguments.epochs,
        arguments.code_dim,
        arguments.use_competitive_layer,
    )
    set_global_seed(seed=seed)

    train_data, test_data = load_dataset(dataset)

    if dataset in ["malimg", "wdbc"]:
        num_features = train_data.tensors[0].shape[1]
        test_size = test_data.tensors[0].shape[0]
        train_size = train_data.tensors[0].shape[0]
    else:
        num_features = np.prod(train_data.data.shape[1:])
        test_size = test_data.data.shape[0]
        train_size = train_data.data.shape[0]

    train_loader = create_dataloader(train_data, batch_size=train_size, num_workers=4)
    test_loader = create_dataloader(test_data, batch_size=test_size)

    autoencoder = Autoencoder(
        input_shape=num_features,
        code_dim=code_dim,
        use_competitive_layer=use_competitive_layer,
    )
    autoencoder.load_model(f"Autoencoder-{dataset}-{code_dim}-dim-competitive_layer")
    autoencoder = autoencoder.cpu()
    for train_features, train_labels in train_loader:
        train_latent_code = autoencoder.compute_latent_code(train_features)
    for test_features, test_labels in test_loader:
        test_latent_code = autoencoder.compute_latent_code(test_features)
    train_latent_code = train_latent_code.detach()
    test_latent_code = test_latent_code.detach()
    train_data = create_dataset(features=train_latent_code, labels=train_labels)
    test_data = create_dataset(features=test_latent_code, labels=test_labels)
    train_loader = create_dataloader(train_data, batch_size=batch_size, num_workers=4)
    test_loader = create_dataloader(test_data, batch_size=test_size)
    model = SoftmaxRegression(input_dim=code_dim, num_classes=10)
    model.fit(train_loader, epochs=epochs)

    with torch.no_grad():
        model = model.cpu()
        model = model.eval()
        for test_features, test_labels in test_loader:
            outputs = model(test_features)
            correct = (outputs.argmax(1) == test_labels).sum().item()
            accuracy = correct / len(test_labels)
    print(f"Test accuracy: {accuracy:.4f}")


if __name__ == "__main__":
    arguments = parse_args()
    main(arguments)
