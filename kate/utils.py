# k-Competitive Autoencoder
# Copyright (C) 2021  Abien Fred Agarap
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""Utility functions module"""
import pickle
import random
from typing import List

import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
import torch

__author__ = "Abien Fred Agarap"


def set_global_seed(seed: int) -> None:
    """
    Sets the seed value for random number generators.

    Parameter
    ---------
    seed: int
        The seed value to use.
    """
    random.seed(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = True


def export_model(model: torch.nn.Module, filename: str):
    """
    Exports the model to file.

    Parameters
    ----------
    model: torch.nn.Module
        The model to export.
    filename: str
        The filename to use for the model file.
    """
    with open(filename, "wb") as model_file:
        pickle.dump(model, model_file)
    print(f"[INFO] Exported model to {filename}")


def visualize_features(
    features: torch.Tensor, labels: torch.Tensor, classes: List, title: str
) -> None:
    """
    Displays the scatterplot for features.

    Parameters
    ----------
    features: torch.Tensor
        The features to visualize.
    labels: torch.Tensor
        The labels to use for colorizing features.
    classes: List
        The list of dataset classes.
    title: str
        The plot title to use.
    """
    if isinstance(features, torch.Tensor):
        features = features.detach().numpy()
    if isinstance(labels, torch.Tensor):
        labels = labels.numpy()
    sns.set_style("whitegrid")
    _ = plt.figure()
    ax = plt.subplot(111)
    for index in range(len(classes)):
        ax.scatter(
            features[labels == index, 0],
            features[labels == index, 1],
            label=classes[index],
            edgecolors="black",
        )
    box = ax.get_position()
    ax.set_position([box.x0, box.y0 + box.height * 0.1, box.width, box.height * 0.9])
    plt.legend(
        loc="upper center",
        title=title,
        ncol=5,
        bbox_to_anchor=(0.5, -0.05),
        fancybox=True,
        shadow=True,
    )
    plt.show()
