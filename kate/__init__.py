# k-Competitive Autoencoder
# Copyright (C) 2021  Abien Fred Agarap
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""Implementation of competitive layer"""
import random

import numpy as np
import torch

__author__ = "Abien Fred Agarap"


class CompetitiveLayer(torch.nn.Module):
    def __init__(self, top_k: int = 32, factor: float = 6.26):
        """
        Builds the competitive layer.

        Parameters
        ----------
        top_k: int
            The number of winners to get.
        factor: float
            The amplification factor.
        """
        super().__init__()
        self.top_k = top_k
        self.factor = factor
        self.device = torch.device("cuda:0")
        self.to(self.device)

    def forward(self, features: torch.Tensor) -> torch.Tensor:
        """
        Forward pass for competitive layer.

        Parameter
        ---------
        features: torch.Tensor
            The input features.

        Returns
        -------
        result: torch.Tensor
            The output of competitive layer.
        """
        positive_winners = (features + torch.abs(features)) / 2
        negative_winners = (features - torch.abs(features)) / 2
        positive_winners = positive_winners.to(self.device)
        negative_winners = negative_winners.to(self.device)
        positive_values, positive_indices = torch.topk(
            positive_winners, self.top_k // 2
        )
        negative_values, negative_indices = torch.topk(
            -negative_winners, self.top_k - self.top_k // 2
        )
        positive_range = torch.arange(
            0, positive_indices.shape[0], device=self.device
        ).unsqueeze(1)
        negative_range = torch.arange(
            0, negative_indices.shape[0], device=self.device
        ).unsqueeze(1)
        positive_range_repeated = positive_range.repeat((1, (self.top_k // 2)))
        negative_range_repeated = negative_range.repeat(
            (1, (self.top_k - self.top_k // 2))
        )
        positive_values = positive_values.to(self.device)
        positive_indices = positive_indices.to(self.device)
        positive_range_repeated = positive_range_repeated.to(self.device)
        negative_values = negative_values.to(self.device)
        negative_indices = negative_indices.to(self.device)
        negative_range_repeated = negative_range_repeated.to(self.device)
        positive_full_indices = torch.stack(
            [positive_range_repeated, positive_indices], dim=2
        )
        negative_full_indices = torch.stack(
            [negative_range_repeated, negative_indices], dim=2
        )
        positive_full_indices = positive_full_indices.view(-1, 2)
        negative_full_indices = negative_full_indices.view(-1, 2)
        positive_full_indices = torch.LongTensor(positive_full_indices.cpu().numpy())
        negative_full_indices = torch.LongTensor(negative_full_indices.cpu().numpy())
        positive_full_indices = positive_full_indices.to(self.device)
        negative_full_indices = negative_full_indices.to(self.device)
        P_reset = torch.sparse_coo_tensor(
            indices=positive_full_indices.t(),
            values=positive_values.view(-1),
            size=features.shape,
        ).to_dense()
        N_reset = torch.sparse_coo_tensor(
            indices=negative_full_indices.t(),
            values=negative_values.view(-1),
            size=features.shape,
        ).to_dense()
        P_tmp = torch.mul(
            self.factor,
            torch.sum(torch.sub(positive_winners, P_reset), 1, keepdim=True),
        )
        N_tmp = torch.mul(
            self.factor,
            torch.sum(torch.sub(-negative_winners, N_reset), 1, keepdim=True),
        )
        P_reset = torch.sparse_coo_tensor(
            indices=positive_full_indices.t(),
            values=(positive_values + P_tmp).view(-1),
            size=features.shape,
        ).to_dense()
        N_reset = torch.sparse_coo_tensor(
            indices=negative_full_indices.t(),
            values=(negative_values + N_tmp).view(-1),
            size=features.shape,
        ).to_dense()
        result = torch.sub(P_reset, N_reset)
        result = result.to(self.device)
        return result.to(self.device)
