# k-Competitive Autoencoder
# Copyright (C) 2021  Abien Fred Agarap
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""Implementation of competitive layer"""
import os
from typing import List, Tuple

import torch
from kate import CompetitiveLayer

__author__ = "Abien Fred Agarap"


class Autoencoder(torch.nn.Module):
    """
    A feed-forward autoencoder network that optimizes
    binary cross entropy using AdamW optimizer.
    """

    def __init__(
        self,
        input_shape: int,
        code_dim: int,
        learning_rate: float = 1e-3,
        use_competitive_layer: bool = False,
        top_k: int = 32,
        amplification_factor: float = 6.26,
        use_weight_tying: bool = False,
        device: torch.device = torch.device(
            "cuda:0" if torch.cuda.is_available() else "cpu"
        ),
    ):
        """
        Constructs the autoencoder model.

        Parameters
        ----------
        input_shape: int
            The dimensionality of the input features.
        code_dim: int
            The dimensionality of the latent code.
        learning_rate: float
            The learning rate to use for optimization.
        use_competitive_layer: bool
            Whether to use competitive layer or not.
        top_k: int
            The top-k neurons to get.
        amplification_factor: bool
            The amplification factor to use for transferring
            energies to the winner neurons.
        use_weight_tying: bool
            Whether to use weight tying or not.
        device: torch.device
            The device to use for the model computations.
        """
        super().__init__()
        self.encoder = torch.nn.ModuleList(
            [
                torch.nn.Linear(
                    in_features=input_shape, out_features=code_dim, bias=False
                ),
                torch.nn.Tanh(),
                # torch.nn.Linear(in_features=500, out_features=500, bias=False),
                # torch.nn.Tanh(),
                # torch.nn.Linear(in_features=500, out_features=2000, bias=False),
                # torch.nn.Tanh(),
                # torch.nn.Linear(in_features=500, out_features=code_dim, bias=False),
                # torch.nn.Tanh(),
            ]
        )
        self.decoder = torch.nn.ModuleList(
            [
                # torch.nn.Linear(in_features=code_dim, out_features=500, bias=False),
                # torch.nn.Tanh(),
                # torch.nn.Linear(in_features=2000, out_features=500, bias=False),
                # torch.nn.Tanh(),
                # torch.nn.Linear(in_features=500, out_features=500, bias=False),
                # torch.nn.Tanh(),
                torch.nn.Linear(
                    in_features=code_dim, out_features=input_shape, bias=False
                ),
                torch.nn.Sigmoid(),
            ]
        )
        for index, layer in enumerate(self.encoder):
            if isinstance(layer, torch.nn.Linear):
                torch.nn.init.xavier_normal_(layer.weight)
        for index, layer in enumerate(self.decoder):
            if isinstance(layer, torch.nn.Linear):
                torch.nn.init.xavier_normal_(layer.weight)
        self.use_weight_tying = use_weight_tying
        if self.use_weight_tying:
            encoder_weights = []
            for encoder_layer in self.encoder:
                if isinstance(encoder_layer, torch.nn.Linear):
                    encoder_weight = encoder_layer.weight.transpose(0, 1)
                    encoder_weights.append(encoder_weight)
            for index in range(len(self.decoder)):
                if isinstance(self.decoder[index], torch.nn.Linear):
                    self.decoder[index].weight = torch.nn.Parameter(
                        encoder_weights.pop()
                    )
        self.use_competitive_layer = use_competitive_layer
        if self.use_competitive_layer:
            self.competitive_layer = CompetitiveLayer(
                top_k=top_k, factor=amplification_factor
            )
        self.criterion = torch.nn.BCELoss()
        self.optimizer = torch.optim.AdamW(params=self.parameters(), lr=learning_rate)
        self.scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(
            self.optimizer, patience=3, verbose=True, min_lr=0.01, factor=0.2
        )

        self.train_loss = []
        self.device = device
        self.to(self.device)

    def forward(self, features: torch.Tensor) -> torch.Tensor:
        """
        Defines the forward pass by the model.

        Parameter
        ---------
        features: torch.Tensor
            The input features.

        Returns
        -------
        reconstruction: torch.Tensor
            The model output.
        """
        if len(features.shape) > 2:
            features = features.view(features.shape[0], -1)
        activations = {}
        for index, layer in enumerate(self.encoder):
            if index == 0:
                activations[index] = layer(features)
            elif self.use_competitive_layer and not isinstance(layer, torch.nn.Linear):
                activations[index] = self.competitive_layer(
                    layer(activations.get(index - 1))
                )
            else:
                activations[index] = layer(activations.get(index - 1))
        latent_code = activations.get(len(activations) - 1)
        for index, layer in enumerate(self.decoder):
            if index == 0:
                activations[index] = layer(latent_code)
            elif (
                self.use_competitive_layer
                and not isinstance(layer, torch.nn.Linear)
                and (index < (len(self.decoder) - 2))
            ):
                activations[index] = self.competitive_layer(
                    layer(activations.get(index - 1))
                )
            else:
                activations[index] = layer(activations.get(index - 1))
        reconstructions = activations.get(len(activations) - 1)
        return reconstructions

    def epoch_train(self, train_loader: torch.utils.data.DataLoader):
        """
        Trains the model for one epoch.

        Parameter
        ---------
        train_loader: torch.utils.data.DataLoader
            The data loader for the training dataset.

        Returns
        -------
        epoch_loss: float
            The training epoch loss.
        """
        epoch_loss = 0
        for batch_features, _ in train_loader:
            batch_features = batch_features.view(batch_features.shape[0], -1)
            batch_features = batch_features.to(self.device)
            self.optimizer.zero_grad()
            outputs = self(batch_features)
            train_loss = self.criterion(outputs, batch_features)
            train_loss.backward()
            self.optimizer.step()
            epoch_loss += train_loss.item()
        epoch_loss /= len(train_loader)
        return epoch_loss

    def fit(
        self, data_loader: torch.utils.data.DataLoader, epochs: int, show_every: int = 2
    ):
        """
        Trains the model for a given number of epochs.

        Parameters
        ----------
        data_loader: torch.utils.data.DataLoader
            The training dataset.
        epochs: int
            The number of epochs to train the model.
        show_every: int
            The interval between training progress displays.
        """
        for epoch in range(epochs):
            epoch_loss = self.epoch_train(data_loader)
            self.train_loss.append(epoch_loss)
            if (epoch + 1) % show_every == 0:
                print(
                    f"epoch {epoch + 1}/{epochs} : mean loss = {self.train_loss[-1]:.6f}"
                )
            self.scheduler.step(epoch)

    def predict(self, features: torch.Tensor) -> torch.Tensor:
        """
        Returns reconstructed data for the input features.

        Parameter
        ---------
        features: torch.Tensor
            The input features.

        Returns
        -------
        reconstruction: torch.Tensor
            The reconstructed data.
        """
        latent_code = self.compute_latent_code(features)
        activations = {}
        for index, layer in enumerate(self.decoder):
            if index == 0:
                activations[index] = layer(latent_code)
            else:
                activations[index] = layer(activations.get(index - 1))
        reconstruction = activations.get(len(activations) - 1)
        return reconstruction

    def compute_latent_code(self, features: torch.Tensor) -> torch.Tensor:
        """
        Returns the latent code representation for the input features.

        Parameter
        ---------
        features: torch.Tensor
            The input features.

        Returns
        -------
        latent_code: torch.Tensor
            The latent code representation for the input features.
        """
        if len(features.shape) > 2:
            features = features.view(features.shape[0], -1)
        activations = {}
        for index, layer in enumerate(self.encoder):
            if index == 0:
                activations[index] = layer(features)
            else:
                activations[index] = layer(activations.get(index - 1))
        latent_code = activations.get(len(activations) - 1)
        return latent_code

    def save_model(self, filename: str = "Autoencoder-competitive_layer") -> None:
        """
        Exports the (presumably) trained autoencoder model.

        Parameter
        ---------
        filename: str
            The path and filename for the exported model.
        """
        print("[INFO] Exporting trained autoencoder model...")
        filename = f"{filename}-{self.use_competitive_layer}.pth"
        torch.save(self.state_dict(), filename)
        print("[SUCCESS] Trained autoencoder model exported.")

    def load_model(self, filename: str = "Autoencoder-competitive_layer") -> None:
        """
        Loads the trained autoencoder model.

        Parameter
        ---------
        filename: str
            The path to the trained autoencoder model.
        """
        filename = f"{filename}-{self.use_competitive_layer}.pth"
        print("[INFO] Loading the trained autoencoder model...")
        if os.path.isfile(filename):
            self.load_state_dict(torch.load(filename))
            print("[SUCCESS] Trained autoencoder ready for use.")
        else:
            print("[ERROR] Trained model not found.")


class FFNN(torch.nn.Module):
    def __init__(
        self,
        units: List or Tuple = [(784, 500), (500, 10)],
        learning_rate: float = 1e-3,
        device: torch.device = torch.device(
            "cuda:0" if torch.cuda.is_available() else "cpu"
        ),
    ):
        """
        Constructs a feed-forward neural network classifier.

        Parameters
        ----------
        units: list or tuple
            An iterable that consists of the number of units in each hidden layer.
        learning_rate: float
            The learning rate to use for optimization.
        device: torch.device
            The device to use for model computations.
        """
        super().__init__()
        self.layers = torch.nn.ModuleList(
            [
                torch.nn.Linear(in_features=in_features, out_features=out_features)
                for in_features, out_features in units
            ]
        )

        for index, layer in enumerate(self.layers):
            if index < (len(self.layers) - 1) and isinstance(layer, torch.nn.Linear):
                torch.nn.init.kaiming_normal_(layer.weight, nonlinearity="relu")
            elif index == (len(self.layers) - 1) and isinstance(layer, torch.nn.Linear):
                torch.nn.init.xavier_uniform_(layer.weight)
        self.optimizer = torch.optim.AdamW(params=self.parameters(), lr=learning_rate)
        self.train_accuracy = []
        self.train_loss = []
        self.device = device
        self.to(self.device)

    def forward(self, features: torch.Tensor) -> torch.Tensor:
        """
        Defines the forward pass by the model.

        Parameter
        ---------
        features: torch.Tensor
            The input features.

        Returns
        -------
        logits: torch.Tensor
            The model output.
        """
        features = features.view(features.shape[0], -1)
        activations = {}
        for index, layer in enumerate(self.layers):
            if index == 0:
                activations[index] = torch.relu(layer(features))
            elif index == len(self.layers) - 1:
                activations[index] = layer(activations.get(index - 1))
            else:
                activations[index] = torch.relu(layer(activations.get(index - 1)))
        logits = activations.get(len(activations) - 1)
        return logits

    def epoch_train(self, data_loader: torch.utils.data.DataLoader) -> float:
        """
        Trains a model for one epoch.

        Parameters
        ----------
        data_loader: torch.utils.dataloader.DataLoader
            The data loader object that consists of the data pipeline.

        Returns
        -------
        epoch_loss: float
            The epoch loss.
        """
        epoch_loss = 0
        for batch_features, batch_labels in data_loader:
            batch_features = batch_features.view(batch_features.shape[0], -1)
            batch_features = batch_features.to(self.device)
            batch_labels = batch_labels.to(self.device)
            self.optimizer.zero_grad()
            outputs = self(batch_features)
            train_loss = self.criterion(outputs, batch_labels)
            train_loss.backward()
            self.optimizer.step()
            epoch_loss += train_loss.item()
        epoch_loss /= len(data_loader)
        return epoch_loss

    def fit(
        self, data_loader: torch.utils.data.DataLoader, epochs: int, show_every: int = 2
    ):
        """
        Trains the neural network model.

        Parameters
        ----------
        data_loader: torch.utils.dataloader.DataLoader
            The data loader object that consists of the data pipeline.
        epochs: int
            The number of epochs to train the model.
        show_every: int
            Show the training progress every n-th epoch.
        """
        for epoch in range(epochs):
            epoch_loss = self.epoch_train(self, data_loader)
            self.train_loss.append(epoch_loss)
            if (epoch + 1) % show_every == 0:
                print(
                    f"epoch {epoch + 1}/{epochs} : mean loss = {self.train_loss[-1]:.6f}"
                )

    def predict(
        self, features: torch.Tensor, return_likelihoods: bool = False
    ) -> torch.Tensor:
        """
        Returns model classifications

        Parameters
        ----------
        features: torch.Tensor
            The input features to classify.
        return_likelihoods: bool
            Whether to return classes with likelihoods or not.

        Returns
        -------
        predictions: torch.Tensor
            The class likelihood output by the model.
        classes: torch.Tensor
            The class prediction by the model.
        """
        outputs = self.forward(features)
        predictions, classes = torch.max(outputs.data, dim=1)
        return (predictions, classes) if return_likelihoods else classes


class SoftmaxRegression(torch.nn.Module):
    def __init__(self, input_dim: int, num_classes: int, learning_rate: float = 1e-3):
        super().__init__()
        self.linear = torch.nn.Linear(in_features=input_dim, out_features=num_classes)
        self.criterion = torch.nn.CrossEntropyLoss()
        self.optimizer = torch.optim.Adam(self.parameters(), lr=learning_rate)
        self.train_loss = []
        self.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        self.to(self.device)

    def forward(self, features: torch.Tensor) -> torch.Tensor:
        """
        Defines the forward pass by the model.

        Parameter
        ---------
        features: torch.Tensor
            The input features.

        Returns
        -------
        logits: torch.Tensor
            The model output.
        """
        logits = self.linear(features)
        return logits

    def predict(self, features: torch.Tensor) -> torch.Tensor:
        """
        Defines the inference function by the model.

        Parameter
        ---------
        features: torch.Tensor
            The input features.

        Returns
        -------
        predictions: torch.Tensor
            The model output.
        """
        if len(features.shape) > 2:
            features = features.view(features.shape[0], -1)
        logits = self.forward(features)
        predictions = torch.softmax(logits, dim=1)
        return predictions

    def epoch_train(self, data_loader: torch.utils.data.DataLoader) -> float:
        """
        Trains a model for one epoch.

        Parameters
        ----------
        data_loader: torch.utils.dataloader.DataLoader
            The data loader object that consists of the data pipeline.

        Returns
        -------
        epoch_loss: float
            The epoch loss.
        """
        epoch_loss = 0
        for batch_features, batch_labels in data_loader:
            batch_features = batch_features.to(self.device)
            batch_labels = batch_labels.to(self.device)
            batch_features = batch_features.view(batch_features.shape[0], -1)
            self.optimizer.zero_grad()
            outputs = self(batch_features)
            train_loss = self.criterion(outputs, batch_labels)
            train_loss.backward()
            self.optimizer.step()
            epoch_loss += train_loss.item()
        epoch_loss /= len(data_loader)
        return epoch_loss

    def fit(
        self, data_loader: torch.utils.data.DataLoader, epochs: int, show_every: int = 2
    ):
        """
        Trains the `SoftmaxRegression` model.

        Parameters
        ----------
        data_loader: torch.utils.dataloader.DataLoader
            The data loader object that consists of the data pipeline.
        epochs: int
            The number of epochs to train the model.
        show_every: int
            Show the training progress every n-th epoch.
        """
        for epoch in range(epochs):
            epoch_loss = self.epoch_train(data_loader)
            self.train_loss.append(epoch_loss)
            if (epoch + 1) % show_every == 0:
                print(
                    f"epoch {epoch + 1}/{epochs} : mean loss = {self.train_loss[-1]:.6f}"
                )
